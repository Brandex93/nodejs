FROM node:12.18.4 AS test
LABEL maintainer="Dzmitry Kurbyko"
RUN mkdir /viget
WORKDIR /viget
COPY . .
RUN npm install

FROM node:latest
LABEL maintainer="Dzmitry Kurbyko"
RUN mkdir /viget
WORKDIR /viget
COPY . .
COPY --from=test /viget/node_modules node_modules
CMD npm start


